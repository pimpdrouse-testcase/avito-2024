package cache

import (
	"avito-testcase/internal/service/readuserbanner"
	"context"
	"sync"
	"time"
)

type userBannerItem struct {
	value   readuserbanner.Output
	created time.Time
}

func newUserBannerItem(output readuserbanner.Output) userBannerItem {
	it := userBannerItem{
		value:   output,
		created: time.Now(),
	}

	return it
}

type userBannerKey struct {
	input string
	role  string
}

func newUserBannerKey(in *readuserbanner.Input, role string) userBannerKey {
	ky := userBannerKey{
		input: in.String(),
		role:  role,
	}

	return ky
}

type UserBannerCache struct {
	mu            sync.RWMutex
	cache         map[userBannerKey]userBannerItem
	ttl           time.Duration
	clearInterval time.Duration
}

func NewUserBannerCache(ctx context.Context, ttl, clearInterval time.Duration) *UserBannerCache {
	ubc := &UserBannerCache{
		mu:            sync.RWMutex{},
		cache:         make(map[userBannerKey]userBannerItem),
		ttl:           ttl,
		clearInterval: clearInterval,
	}

	go ubc.cacheCleaner(ctx)

	return ubc
}

func (ubc *UserBannerCache) Get(in *readuserbanner.Input, role string) (readuserbanner.Output, bool) {
	ubc.mu.RLock()
	defer ubc.mu.RUnlock()
	k := newUserBannerKey(in, role)

	if val, ok := ubc.cache[k]; ok {
		return val.value, ok
	}

	return nil, false
}

func (ubc *UserBannerCache) Set(in *readuserbanner.Input, role string, val readuserbanner.Output) {
	k := newUserBannerKey(in, role)
	it := newUserBannerItem(val)

	ubc.mu.Lock()
	defer ubc.mu.Unlock()
	ubc.cache[k] = it
}

func (ubc *UserBannerCache) Delete(k userBannerKey) {
	ubc.mu.Lock()
	defer ubc.mu.Unlock()

	delete(ubc.cache, k)
}

func (ubc *UserBannerCache) cacheCleaner(ctx context.Context) {
	for {
		select {
		case <-ctx.Done():
			return
		default:
			for k, v := range ubc.cache {
				if time.Since(v.created) >= ubc.ttl {
					ubc.Delete(k)
				}
			}
			<-time.After(ubc.clearInterval)
		}
	}
}
