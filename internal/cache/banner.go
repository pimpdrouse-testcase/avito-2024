package cache

import (
	"avito-testcase/internal/service/readbanner"
	"context"
	"sync"
	"time"
)

type bannerItem struct {
	value   *readbanner.Output
	created time.Time
}

func newBannerItem(output *readbanner.Output) bannerItem {
	it := bannerItem{
		value:   output,
		created: time.Now(),
	}

	return it
}

type bannerKey struct {
	input string
	role  string
}

func newBannerKey(in *readbanner.Input, role string) bannerKey {
	k := bannerKey{
		input: in.String(),
		role:  role,
	}

	return k
}

type BannerCache struct {
	mu            sync.RWMutex
	cache         map[bannerKey]bannerItem
	ttl           time.Duration
	clearInterval time.Duration
}

func NewBannerCache(ctx context.Context, ttl, clearInterval time.Duration) *BannerCache {
	bc := &BannerCache{
		mu:            sync.RWMutex{},
		cache:         make(map[bannerKey]bannerItem),
		ttl:           ttl,
		clearInterval: clearInterval,
	}

	go bc.cacheCleaner(ctx)

	return bc
}

func (bc *BannerCache) Get(in *readbanner.Input, role string) (*readbanner.Output, bool) {
	bc.mu.RLock()
	defer bc.mu.RUnlock()
	k := newBannerKey(in, role)

	if val, ok := bc.cache[k]; ok {
		return val.value, ok
	}

	return nil, false
}

func (bc *BannerCache) Set(in *readbanner.Input, role string, val *readbanner.Output) {
	k := newBannerKey(in, role)
	it := newBannerItem(val)

	bc.mu.Lock()
	defer bc.mu.Unlock()
	bc.cache[k] = it
}

func (bc *BannerCache) Delete(k bannerKey) {
	bc.mu.Lock()
	defer bc.mu.Unlock()

	delete(bc.cache, k)
}

func (bc *BannerCache) cacheCleaner(ctx context.Context) {
	for {
		select {
		case <-ctx.Done():
			return
		default:
			for k, v := range bc.cache {
				if time.Since(v.created) >= bc.ttl {
					bc.Delete(k)
				}
			}
			<-time.After(bc.clearInterval)
		}
	}
}
