package config

import (
	"github.com/go-playground/validator/v10"
	"gopkg.in/yaml.v3"
	"io"
	"os"
	"time"
)

type Config struct {
	AppAddress         string        `yaml:"app_address" validate:"required,hostname_port"`
	PgURL              string        `yaml:"pg_url" validate:"required,url"`
	CacheTTL           time.Duration `yaml:"cache_ttl"`
	CacheClearInterval time.Duration `yaml:"cache_clear_interval"`
}

func NewConfig(in io.Reader) (*Config, error) {
	cfg := &Config{}

	dec := yaml.NewDecoder(in)
	dec.KnownFields(true)
	if err := dec.Decode(cfg); err != nil {
		return nil, err
	}

	if val, ok := os.LookupEnv("APP_ADDRESS"); ok {
		cfg.AppAddress = val
	}

	if val, ok := os.LookupEnv("PG_URL"); ok {
		cfg.PgURL = val
	}

	if val, ok := os.LookupEnv("CACHE_TTL"); ok {
		ttl, err := time.ParseDuration(val)
		if err != nil {
			return nil, err
		}
		cfg.CacheTTL = ttl
	}

	if val, ok := os.LookupEnv("CACHE_CLEAR_INTERVAL"); ok {
		clearInt, err := time.ParseDuration(val)
		if err != nil {
			return nil, err
		}
		cfg.CacheClearInterval = clearInt
	}

	return cfg, nil

}

func (cfg *Config) Validate() error {
	return validator.New(validator.WithRequiredStructEnabled()).Struct(cfg)
}
