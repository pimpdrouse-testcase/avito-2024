package userbanner

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestGetValidation(t *testing.T) {
	tests := []struct {
		name     string
		target   string
		expected int
	}{
		{
			name:     "tag_id not valid test",
			target:   "/userbanner?tag_id=-1&feature_id=5",
			expected: http.StatusBadRequest,
		},
		{
			name:     "not tag_id test",
			target:   "/userbanner?feature_id=5",
			expected: http.StatusBadRequest,
		},
		{
			name:     "feature_id not valid test",
			target:   "/userbanner?tag_id=100&feature_id=-1",
			expected: http.StatusBadRequest,
		},
		{
			name:     "not feature_id test",
			target:   "/userbanner?tag_id=5",
			expected: http.StatusBadRequest,
		},
		{
			name:     "success test with use_last_revision",
			target:   "/userbanner?tag_id=5&feature_id=10&use_last_revision",
			expected: http.StatusOK,
		},
		{
			name:     "success test without use_last_revision",
			target:   "/userbanner?tag_id=5&feature_id=10",
			expected: http.StatusOK,
		},
	}

	eng := gin.Default()
	eng.GET("/userbanner", GetValidation)

	for _, test := range tests {
		t.Run(test.name,
			func(t *testing.T) {

				w := httptest.NewRecorder()
				req := httptest.NewRequest(http.MethodGet, test.target, nil)

				eng.ServeHTTP(w, req)

				if w.Code != test.expected {
					t.Errorf("Test failed because expected not equal result: %v, %v, %v", test.expected, w.Code, w.Body.String())
				}
			},
		)
	}

}
