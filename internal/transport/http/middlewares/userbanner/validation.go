package userbanner

import (
	"avito-testcase/internal/service/readuserbanner"
	Error "avito-testcase/internal/transport/http/_helpers/errorz"
	"github.com/gin-gonic/gin"
	"net/http"
)

func GetValidation(c *gin.Context) {
	data := new(readuserbanner.Input)
	if err := c.BindQuery(data); err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, Error.FromError(err))
		return
	}

	if err := data.Validate(); err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, Error.FromError(err))
		return
	}

	c.Next()
}
