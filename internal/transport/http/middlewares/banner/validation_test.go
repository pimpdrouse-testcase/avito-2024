package banner

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

func TestGetBannerValidation(t *testing.T) {
	tests := []struct {
		name     string
		target   string
		expected int
	}{
		{
			name:     "tag_id not valid test",
			target:   "/banner?tag_id=-10",
			expected: http.StatusBadRequest,
		},
		{
			name:     "feature_id not valid test",
			target:   "/banner?feature_id=-5",
			expected: http.StatusBadRequest,
		},
		{
			name:     "limit not valid test",
			target:   "/banner?limit=-1",
			expected: http.StatusBadRequest,
		},
		{
			name:     "offset not valid test",
			target:   "/banner?offset=-5",
			expected: http.StatusBadRequest,
		},
		{
			name:     "success test with all",
			target:   "/banner?tag_id=0&feature_id=0&limit=0&offset=10",
			expected: http.StatusOK,
		},
	}

	eng := gin.Default()
	eng.GET("/banner", GetValidation)

	for _, test := range tests {
		t.Run(test.name,
			func(t *testing.T) {

				w := httptest.NewRecorder()
				req := httptest.NewRequest(http.MethodGet, test.target, nil)

				eng.ServeHTTP(w, req)

				if w.Code != test.expected {
					t.Errorf("Test failed because expected not equal result: %v, %v, %v", test.expected, w.Code, w.Body.String())
				}
			},
		)
	}
}

func TestPostBannerValidation(t *testing.T) {
	tests := []struct {
		name     string
		body     string
		expected int
	}{
		{
			name:     "example request test",
			body:     `{"tag_ids":[0],"feature_id":1,"content":"{\"title\": \"some_title\", \"text\": \"some_text\", \"url\": \"some_url\"}","is_active":true}`,
			expected: http.StatusOK,
		},
		{
			name:     "tags_id length 0 test",
			body:     `{"tag_ids":[],"feature_id":0,"content":"{\"title\": \"some_title\", \"text\": \"some_text\", \"url\": \"some_url\"}","is_active":true}`,
			expected: http.StatusBadRequest,
		},
		{
			name:     "feature_id not valid test",
			body:     `{"tag_ids":[0],"feature_id":-1,"content":"{\"title\": \"some_title\", \"text\": \"some_text\", \"url\": \"some_url\"}","is_active":true}`,
			expected: http.StatusBadRequest,
		},
		{
			name:     "content not valid test",
			body:     `{"tag_ids":[0],"feature_id":0,"content":"title: "some_title"","is_active":true}`,
			expected: http.StatusBadRequest,
		},
		{
			name:     "is_active not valid test",
			body:     `{"tag_ids":[0],"feature_id":0,"content":"{\"title\": \"some_title\", \"text\": \"some_text\", \"url\": \"some_url\"}","is_active":"arrbaba"}`,
			expected: http.StatusBadRequest,
		},
		{
			name:     "non one field test",
			body:     `{"tag_ids":[0],"feature_id":0,"content":"{\"title\": \"some_title\", \"text\": \"some_text\", \"url\": \"some_url\"}"}`,
			expected: http.StatusBadRequest,
		},
	}

	eng := gin.Default()
	eng.POST("/banner", PostValidation)

	for _, test := range tests {
		t.Run(test.name,
			func(t *testing.T) {

				w := httptest.NewRecorder()
				req := httptest.NewRequest(http.MethodPost, "/banner", strings.NewReader(test.body))
				req.Header.Add("Content-Type", "application/json")

				eng.ServeHTTP(w, req)

				if w.Code != test.expected {
					t.Errorf("Test failed because expected not equal result: %v, %v, %v", test.expected, w.Code, w.Body.String())
				}
			},
		)
	}
}

func TestPatchBannerValidation(t *testing.T) {
	tests := []struct {
		name     string
		id       string
		body     string
		expected int
	}{
		{
			name:     "example request test",
			id:       "1",
			body:     `{"tag_ids":[0],"feature_id":1,"content":"{\"title\": \"some_title\", \"text\": \"some_text\", \"url\": \"some_url\"}","is_active":true}`,
			expected: http.StatusOK,
		},
		{
			name:     "id not valid test",
			id:       "aaa",
			body:     `{"tag_ids":[0],"feature_id":1,"content":"{\"title\": \"some_title\", \"text\": \"some_text\", \"url\": \"some_url\"}","is_active":true}`,
			expected: http.StatusBadRequest,
		},
		{
			name:     "id lower then 0 test",
			id:       "-1",
			body:     `{"tag_ids":[0],"feature_id":1,"content":"{\"title\": \"some_title\", \"text\": \"some_text\", \"url\": \"some_url\"}","is_active":true}`,
			expected: http.StatusBadRequest,
		},
		{
			name:     "tags_ids elems lower then 0 test",
			id:       "1",
			body:     `{"tag_ids":[-1],"feature_id":0,"content":"{\"title\": \"some_title\", \"text\": \"some_text\", \"url\": \"some_url\"}","is_active":true}`,
			expected: http.StatusBadRequest,
		},
		{
			name:     "feature_id not valid test",
			id:       "1",
			body:     `{"tag_ids":[0],"feature_id":-1,"content":"{\"title\": \"some_title\", \"text\": \"some_text\", \"url\": \"some_url\"}","is_active":true}`,
			expected: http.StatusBadRequest,
		},
		{
			name:     "content not valid test",
			id:       "1",
			body:     `{"tag_ids":[0],"feature_id":0,"content":"title: "some_title"","is_active":true}`,
			expected: http.StatusBadRequest,
		},
		{
			name:     "is_active not valid test",
			id:       "1",
			body:     `{"tag_ids":[0],"feature_id":0,"content":"{\"title\": \"some_title\", \"text\": \"some_text\", \"url\": \"some_url\"}","is_active":"arrbaba"}`,
			expected: http.StatusBadRequest,
		},
		{
			name:     "non one field test",
			id:       "1",
			body:     `{"tag_ids":[0],"feature_id":0,"content":"{\"title\": \"some_title\", \"text\": \"some_text\", \"url\": \"some_url\"}"}`,
			expected: http.StatusOK,
		},
	}

	eng := gin.Default()
	eng.PATCH("/banner/:id", PatchValidation)

	for _, test := range tests {
		t.Run(test.name,
			func(t *testing.T) {

				w := httptest.NewRecorder()
				req := httptest.NewRequest(http.MethodPatch, fmt.Sprintf("/banner/%v", test.id), strings.NewReader(test.body))
				req.Header.Add("Content-Type", "application/json")

				eng.ServeHTTP(w, req)

				if w.Code != test.expected {
					t.Errorf("Test failed because expected not equal result: %v, %v, %v", test.expected, w.Code, w.Body.String())
				}
			},
		)
	}
}

func TestDeleteBannerValidation(t *testing.T) {
	tests := []struct {
		name     string
		id       string
		expected int
	}{
		{
			name:     "example request test",
			id:       "1",
			expected: http.StatusOK,
		},
		{
			name:     "id not valid test",
			id:       "aaa",
			expected: http.StatusBadRequest,
		},
		{
			name:     "id lower then 0 test",
			id:       "-1",
			expected: http.StatusBadRequest,
		},
	}

	eng := gin.Default()
	eng.DELETE("/banner/:id", DeleteValidation)

	for _, test := range tests {
		t.Run(test.name,
			func(t *testing.T) {

				w := httptest.NewRecorder()
				req := httptest.NewRequest(http.MethodDelete, fmt.Sprintf("/banner/%v", test.id), nil)
				req.Header.Add("Content-Type", "application/json")

				eng.ServeHTTP(w, req)

				if w.Code != test.expected {
					t.Errorf("Test failed because expected not equal result: %v, %v, %v", test.expected, w.Code, w.Body.String())
				}
			},
		)
	}
}
