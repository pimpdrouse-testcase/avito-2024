package banner

import (
	"avito-testcase/internal/service/createbanner"
	"avito-testcase/internal/service/deletebanner"
	"avito-testcase/internal/service/readbanner"
	"avito-testcase/internal/service/updatebanner"
	Error "avito-testcase/internal/transport/http/_helpers/errorz"
	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	"net/http"
	"strconv"
)

func GetValidation(c *gin.Context) {
	data := readbanner.NewInput()
	if err := c.BindQuery(data); err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, Error.FromError(err))
		return
	}

	if err := data.Validate(); err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, Error.FromError(err))
		return
	}

	c.Next()
}

func PostValidation(c *gin.Context) {
	if c.GetHeader("Content-Type") != "application/json" {
		c.AbortWithStatusJSON(http.StatusBadRequest, Error.FromString("Content-Type is not application/json"))
		return
	}

	data := createbanner.NewInput()
	if err := c.ShouldBindBodyWith(data, binding.JSON); err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, Error.FromError(err))
		return
	}

	if err := data.Validate(); err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, Error.FromError(err))
		return
	}

	c.Next()
}

func PatchValidation(c *gin.Context) {
	if c.GetHeader("Content-Type") != "application/json" {
		c.AbortWithStatusJSON(http.StatusBadRequest, Error.FromString("Content-Type is not json"))
		return
	}

	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, Error.FromError(err))
		return
	}

	data := updatebanner.NewInput(id)

	if err := c.ShouldBindBodyWith(data, binding.JSON); err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, Error.FromError(err))
		return
	}

	if err = data.Validate(); err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, Error.FromError(err))
		return
	}

	c.Next()
}

func DeleteValidation(c *gin.Context) {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, Error.FromError(err))
		return
	}

	data := deletebanner.NewInput(id)

	if err = data.Validate(); err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, Error.FromError(err))
		return
	}

	c.Next()
}
