package middlewares

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestTokenValidator(t *testing.T) {
	tests := []struct {
		name     string
		token    string
		expected int
	}{
		{
			name:     "Admin role validate",
			token:    "admin_token",
			expected: http.StatusOK,
		},
		{
			name:     "User role validate",
			token:    "user_token",
			expected: http.StatusOK,
		},
		{
			name:     "Not valid role validate",
			token:    "unknown_token",
			expected: http.StatusUnauthorized,
		},
		{
			name:     "Not valid token validate",
			token:    "fdsgfds",
			expected: http.StatusUnauthorized,
		},
	}

	eng := gin.New()
	eng.GET("/", TokenValidator)

	for _, test := range tests {
		t.Run(test.name,
			func(t *testing.T) {

				w := httptest.NewRecorder()
				req := httptest.NewRequest(http.MethodGet, "/", nil)
				req.Header.Set("Token", test.token)

				eng.ServeHTTP(w, req)

				if w.Code != test.expected {
					t.Errorf("Test failed because expected not equal result: %v, %v, %v", test.expected, w.Code, w.Body.String())
				}
			},
		)
	}

}
