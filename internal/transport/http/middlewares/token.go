package middlewares

import (
	"avito-testcase/internal/transport/http/_helpers/token"
	"github.com/gin-gonic/gin"
	"net/http"
)

func TokenValidator(c *gin.Context) {
	tok, err := token.FromString(c.GetHeader("token"))
	if err != nil {
		c.AbortWithStatus(http.StatusUnauthorized)
		return
	}

	if err = tok.Validate(); err != nil {
		c.AbortWithStatus(http.StatusUnauthorized)
		return
	}

	c.AddParam("role", tok.Role)

	c.Next()
}
