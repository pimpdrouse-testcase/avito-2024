package token

import (
	"fmt"
	"github.com/go-playground/validator/v10"
	"strings"
)

type Token struct {
	Role string `validate:"required,alpha,gte=3,oneof=admin user"`
	User string `validate:"required,alphanum,gte=3"`
}

func FromString(token string) (*Token, error) {
	res := strings.Split(token, "_")
	if ln := len(res); ln != 2 {
		return nil, fmt.Errorf("token length not valid: %v", ln)
	}

	tok := &Token{
		Role: res[0],
		User: res[0],
	}

	return tok, nil
}

func (t *Token) Validate() error {
	return validator.New(validator.WithRequiredStructEnabled()).Struct(t)
}
