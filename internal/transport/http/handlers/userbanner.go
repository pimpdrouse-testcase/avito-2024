package handlers

import (
	"avito-testcase/internal/service/readuserbanner"
	"avito-testcase/internal/transport/http/_helpers/errorz"
	"errors"
	"github.com/gin-gonic/gin"
	"net/http"
)

type UserBannerHandler struct {
	readController *readuserbanner.Controller
}

func NewUserBannerHandler(controller *readuserbanner.Controller) *UserBannerHandler {
	ubh := &UserBannerHandler{
		readController: controller,
	}

	return ubh
}

func (ubh *UserBannerHandler) Get(c *gin.Context) {
	data := new(readuserbanner.Input)
	_ = c.BindQuery(data)

	role := c.Param("role")

	res, err := ubh.readController.Read(data, role)
	if errors.Is(err, readuserbanner.ErrBannerNotFound) {
		c.Status(http.StatusNotFound)
		return
	}

	if err != nil {
		c.JSON(http.StatusInternalServerError, errorz.FromError(err))
		return
	}

	c.JSON(http.StatusOK, res)
}
