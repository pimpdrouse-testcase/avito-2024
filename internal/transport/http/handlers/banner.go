package handlers

import (
	"avito-testcase/internal/service/createbanner"
	"avito-testcase/internal/service/deletebanner"
	"avito-testcase/internal/service/readbanner"
	"avito-testcase/internal/service/updatebanner"
	"avito-testcase/internal/transport/http/_helpers/errorz"
	"errors"
	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	"net/http"
	"strconv"
)

type BannerHandler struct {
	readController   *readbanner.Controller
	createController *createbanner.Controller
	updateController *updatebanner.Controller
	deleteController *deletebanner.Controller
}

func NewBannerHandler(read *readbanner.Controller, create *createbanner.Controller, update *updatebanner.Controller, delete *deletebanner.Controller) *BannerHandler {
	bh := &BannerHandler{
		readController:   read,
		createController: create,
		updateController: update,
		deleteController: delete,
	}

	return bh
}

func (bh *BannerHandler) Get(c *gin.Context) {
	data := readbanner.NewInput()
	_ = c.BindQuery(data)

	role := c.Param("role")

	res, err := bh.readController.Read(data, role)

	if err != nil {
		c.JSON(http.StatusInternalServerError, errorz.FromError(err))
		return
	}

	c.JSON(http.StatusOK, res)

}

func (bh *BannerHandler) Post(c *gin.Context) {
	data := createbanner.NewInput()
	err := c.ShouldBindBodyWith(data, binding.JSON)
	if err != nil {
		c.JSON(http.StatusInternalServerError, errorz.FromError(err))
		return
	}

	role := c.Param("role")

	res, err := bh.createController.Create(data, role)
	if errors.Is(err, createbanner.ErrUserForbidden) {
		c.Status(http.StatusForbidden)
		return
	}

	if err != nil {
		c.JSON(http.StatusInternalServerError, errorz.FromError(err))
		return
	}

	c.JSON(http.StatusCreated, res)
}

func (bh *BannerHandler) Patch(c *gin.Context) {
	id, _ := strconv.Atoi(c.Param("id"))
	data := updatebanner.NewInput(id)

	if err := c.ShouldBindBodyWith(data, binding.JSON); err != nil {
		c.JSON(http.StatusInternalServerError, errorz.FromError(err))
		return
	}

	role := c.Param("role")

	err := bh.updateController.Update(data, role)
	if errors.Is(err, updatebanner.ErrUserForbidden) {
		c.Status(http.StatusForbidden)
		return
	}

	if errors.Is(err, updatebanner.ErrBannerNotFound) {
		c.Status(http.StatusNotFound)
		return
	}

	if err != nil {
		c.JSON(http.StatusInternalServerError, errorz.FromError(err))
		return
	}

	c.Status(http.StatusOK)
}

func (bh *BannerHandler) Delete(c *gin.Context) {
	id, _ := strconv.Atoi(c.Param("id"))
	data := deletebanner.NewInput(id)

	role := c.Param("role")

	err := bh.deleteController.Delete(data, role)
	if errors.Is(err, deletebanner.ErrUserForbidden) {
		c.Status(http.StatusForbidden)
		return
	}

	if errors.Is(err, deletebanner.ErrBannerNotFound) {
		c.Status(http.StatusNotFound)
		return
	}

	if err != nil {
		c.JSON(http.StatusInternalServerError, errorz.FromError(err))
		return
	}

	c.Status(http.StatusNoContent)
}
