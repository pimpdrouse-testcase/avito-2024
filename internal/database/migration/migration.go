package migration

import (
	"errors"
	"github.com/golang-migrate/migrate/v4"
	_ "github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"
)

type Migration struct {
	PgURL string
}

func NewMigration(pgURL string) *Migration {
	mig := &Migration{
		PgURL: pgURL,
	}

	return mig
}

func (mig *Migration) Migrate() error {
	mg, err := migrate.New("file://migrations", mig.PgURL)
	if err != nil {
		return err
	}

	if err = mg.Up(); err != nil && !errors.Is(err, migrate.ErrNoChange) {
		return err
	}

	return nil
}
