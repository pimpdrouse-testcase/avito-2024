package userbannerdb

import (
	"avito-testcase/internal/service/readuserbanner"
	"database/sql"
	"errors"
	"log/slog"
)

type PostgresUserBanner struct {
	connectionString string
}

func NewPostgresUserBanner(connStr string) *PostgresUserBanner {
	pgb := &PostgresUserBanner{
		connectionString: connStr,
	}

	return pgb
}

func (pgub *PostgresUserBanner) Read(in *readuserbanner.Input, role string) (readuserbanner.Output, error) {
	conn, err := sql.Open("pgx", pgub.connectionString)
	if err != nil {
		return nil, err
	}
	defer func() {
		if err = conn.Close(); !errors.Is(err, sql.ErrConnDone) && err != nil {
			slog.Error("Close postgres connection error: ", err)
		}
	}()

	query := NewReadUserBanner(conn, in, role)

	return query.SendQuery()
}
