package userbannerdb

import (
	"avito-testcase/internal/service/readuserbanner"
	"database/sql"
	"encoding/json"
	"errors"
)

const adminQuery = `SELECT content FROM banners ban 
    				JOIN tag_banner tag_ban ON tag_ban.banner_id = ban.id AND tag_ban.tag_id = $1
					WHERE ban.feature_id = $2`

const standardQuery = `SELECT content FROM banners ban 
    				JOIN tag_banner tag_ban ON tag_ban.banner_id = ban.id AND tag_ban.tag_id = $1
					WHERE ban.feature_id = $2 AND ban.is_active`

type ReadUserBanner struct {
	conn *sql.DB
	in   *readuserbanner.Input
	role string
}

func NewReadUserBanner(conn *sql.DB, in *readuserbanner.Input, role string) *ReadUserBanner {
	rub := &ReadUserBanner{
		conn: conn,
		in:   in,
		role: role,
	}

	return rub
}

func (rub *ReadUserBanner) SendQuery() (readuserbanner.Output, error) {
	out := readuserbanner.NewOutput()
	var query string

	if rub.role == "admin" {
		query = adminQuery
	} else {
		query = standardQuery
	}

	row := rub.conn.QueryRow(query, *rub.in.TagID, *rub.in.FeatureID)

	var cont []byte
	err := row.Scan(&cont)
	if errors.Is(err, sql.ErrNoRows) {
		return nil, readuserbanner.ErrBannerNotFound
	}
	if err != nil {
		return nil, err
	}

	_ = json.Unmarshal(cont, &out)

	return out, err
}
