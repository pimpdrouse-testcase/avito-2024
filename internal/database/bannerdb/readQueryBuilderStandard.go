package bannerdb

import (
	"avito-testcase/internal/service/readbanner"
	"strconv"
	"strings"
)

type StandardQueryBuilder struct {
}

func (sqb StandardQueryBuilder) Build(data *readbanner.Input) string {
	var queryBuilder strings.Builder

	queryBuilder.WriteString(sqb.initQuery())
	queryBuilder.WriteString(sqb.featureFilter(data.FeatureID))
	queryBuilder.WriteString(sqb.tagFilter(data.TagID))
	queryBuilder.WriteString(sqb.activeFilter())
	queryBuilder.WriteString(sqb.limit(data.Limit))
	queryBuilder.WriteString(sqb.offset(data.Offset))

	return queryBuilder.String()
}

func (sqb StandardQueryBuilder) initQuery() string {
	return `SELECT DISTINCT ON (bans.id) bans.id , bans.feature_id, bans.content, bans.is_active, bans.created_at, bans.updated_at 
			FROM banners bans `
}

func (sqb StandardQueryBuilder) featureFilter(featureID *int) string {
	featureBuilder := strings.Builder{}
	featureBuilder.WriteString("JOIN features feat ON bans.feature_id = feat.id ")
	if featureID != nil {
		featureBuilder.WriteString("AND feat.id = ")
		featureBuilder.WriteString(strconv.Itoa(*featureID))
		featureBuilder.WriteString(" ")
	}

	return featureBuilder.String()
}

func (sqb StandardQueryBuilder) tagFilter(tagID *int) string {
	featureBuilder := strings.Builder{}
	featureBuilder.WriteString("JOIN tag_banner tag_ban ON bans.id = tag_ban.banner_id ")
	if tagID != nil {
		featureBuilder.WriteString("AND tag_ban.tag_id = ")
		featureBuilder.WriteString(strconv.Itoa(*tagID))
		featureBuilder.WriteString(" ")
	}

	return featureBuilder.String()
}

func (sqb StandardQueryBuilder) activeFilter() string {
	return "WHERE bans.is_active = true "
}

func (sqb StandardQueryBuilder) limit(limit *int) string {
	limitBuilder := strings.Builder{}
	if limit != nil {
		limitBuilder.WriteString("LIMIT ")
		limitBuilder.WriteString(strconv.Itoa(*limit))
		limitBuilder.WriteString(" ")
	}

	return limitBuilder.String()
}

func (sqb StandardQueryBuilder) offset(offset *int) string {
	offsetBuilder := strings.Builder{}
	if offset != nil {
		offsetBuilder.WriteString("OFFSET ")
		offsetBuilder.WriteString(strconv.Itoa(*offset))
		offsetBuilder.WriteString(" ")
	}

	return offsetBuilder.String()
}
