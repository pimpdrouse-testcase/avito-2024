package bannerdb

import (
	"avito-testcase/internal/service/createbanner"
	"avito-testcase/internal/service/deletebanner"
	"avito-testcase/internal/service/readbanner"
	"avito-testcase/internal/service/updatebanner"
	"database/sql"
	"errors"
	_ "github.com/jackc/pgx/v5/stdlib"
	"log/slog"
)

type PostgresBanner struct {
	connectionString string
}

func NewPostgresBanner(connStr string) *PostgresBanner {
	pgb := &PostgresBanner{
		connectionString: connStr,
	}

	return pgb
}

func (pgb *PostgresBanner) Create(in *createbanner.Input) (*createbanner.Output, error) {
	conn, err := sql.Open("pgx", pgb.connectionString)
	if err != nil {
		return nil, err
	}
	defer func() {
		if err := conn.Close(); err != nil {
			slog.Error("Close connection on delete op error: ", err)
		}
	}()

	tx, err := conn.Begin()
	if err != nil {
		return nil, err
	}
	defer func() {
		if err = tx.Rollback(); !errors.Is(err, sql.ErrTxDone) && err != nil {
			slog.Error("Rollback transaction on delete op error: ", err)
		}
	}()

	query := NewCreateBanner(tx, in)

	out, err := query.SendQuery()
	if err != nil {
		return nil, err
	}

	if err = tx.Commit(); err != nil {
		slog.Error("Transaction transaction on delete op error: ", err)
	}

	return out, nil

}

func (pgb *PostgresBanner) Read(in *readbanner.Input, role string) (*readbanner.Output, error) {
	conn, err := sql.Open("pgx", pgb.connectionString)
	if err != nil {
		return nil, err
	}
	defer func() {
		if err = conn.Close(); !errors.Is(err, sql.ErrConnDone) && err != nil {
			slog.Error("Close postgres connection error: ", err)
		}
	}()

	query := NewReadBanners(conn, in, role)

	return query.SendQuery()
}

func (pgb *PostgresBanner) Update(in *updatebanner.Input) error {
	conn, err := sql.Open("pgx", pgb.connectionString)
	if err != nil {
		return err
	}
	defer func() {
		if err := conn.Close(); err != nil {
			slog.Error("Close connection on delete op error: ", err)
		}
	}()

	tx, err := conn.Begin()
	if err != nil {
		return err
	}
	defer func() {
		if err = tx.Rollback(); !errors.Is(err, sql.ErrTxDone) && err != nil {
			slog.Error("Rollback transaction on delete op error: ", err)
		}
	}()

	query := NewUpdateBanner(tx, in)

	if err = query.SendQuery(); err != nil {
		return err
	}

	if err = tx.Commit(); err != nil {
		slog.Error("Transaction transaction on delete op error: ", err)
	}

	return nil
}

func (pgb *PostgresBanner) Delete(in *deletebanner.Input) error {
	conn, err := sql.Open("pgx", pgb.connectionString)
	if err != nil {
		return err
	}
	defer func() {
		if err := conn.Close(); err != nil {
			slog.Error("Close connection on delete op error: ", err)
		}
	}()

	tx, err := conn.Begin()
	if err != nil {
		return err
	}
	defer func() {
		if err = tx.Rollback(); !errors.Is(err, sql.ErrTxDone) && err != nil {
			slog.Error("Rollback transaction on delete op error: ", err)
		}
	}()

	query := NewDeleteBanner(tx, in)

	if err = query.SendQuery(); err != nil {
		return err
	}

	if err = tx.Commit(); err != nil {
		slog.Error("Transaction transaction on delete op error: ", err)
	}

	return nil
}
