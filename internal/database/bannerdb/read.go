package bannerdb

import (
	"avito-testcase/internal/service/readbanner"
	"database/sql"
	"encoding/json"
)

type ReadQueryBuilder interface {
	Build(in *readbanner.Input) string
}

type ReadBanners struct {
	conn *sql.DB
	in   *readbanner.Input
	role string
}

func NewReadBanners(conn *sql.DB, in *readbanner.Input, role string) *ReadBanners {
	rb := &ReadBanners{
		conn: conn,
		in:   in,
		role: role,
	}

	return rb
}

func (rb *ReadBanners) SendQuery() (*readbanner.Output, error) {
	out := readbanner.Output{}

	var query ReadQueryBuilder

	switch rb.role {
	case "admin":
		query = AdminQueryBuilder{}
	default:
		query = StandardQueryBuilder{}
	}

	rows, err := rb.conn.Query(query.Build(rb.in))
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		ban := readbanner.Banner{}

		var content []byte
		if err = rows.Scan(&ban.BannerID, &ban.FeatureID, &content, &ban.IsActive, &ban.CreatedAt, &ban.UpdatedAt); err != nil {
			return nil, err
		}

		err = json.Unmarshal(content, &ban.Content)
		if err != nil {
			return nil, err
		}

		ban.TagIDs, err = rb.tagsID(ban.BannerID)
		if err != nil {
			return nil, err
		}

		out = append(out, ban)
	}

	return &out, nil
}

func (rb *ReadBanners) tagsID(bannerID int) ([]int, error) {
	var tagsID []int

	ids, err := rb.conn.Query(`
			SELECT tag.id 
			FROM  tag_banner tag_ban
				JOIN tags tag ON tag.id = tag_ban.tag_id  AND tag_ban.banner_id = $1`, bannerID)

	if err != nil {
		return nil, err
	}

	for ids.Next() {
		id := 0
		if err = ids.Scan(&id); err != nil {
			return nil, err
		}
		tagsID = append(tagsID, id)
	}

	if err = ids.Err(); err != nil {
		return nil, err
	}

	return tagsID, err
}
