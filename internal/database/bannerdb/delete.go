package bannerdb

import (
	"avito-testcase/internal/service/deletebanner"
	"database/sql"
)

type DeleteBanner struct {
	tx *sql.Tx
	in *deletebanner.Input
}

func NewDeleteBanner(tx *sql.Tx, in *deletebanner.Input) *DeleteBanner {
	db := &DeleteBanner{
		tx: tx,
		in: in,
	}

	return db
}

func (db *DeleteBanner) SendQuery() error {
	var err error
	var cnt int

	row := db.tx.QueryRow("SELECT count(*) FROM banners WHERE id = $1", db.in.BannerID)

	err = row.Scan(&cnt)
	if err != nil {
		return err
	}

	if cnt == 0 {
		return deletebanner.ErrBannerNotFound
	}

	_, err = db.tx.Exec("DELETE FROM tag_banner WHERE banner_id = $1", db.in.BannerID)
	if err != nil {
		return err
	}

	_, err = db.tx.Exec("DELETE FROM banners WHERE id = $1", db.in.BannerID)
	if err != nil {
		return err
	}

	return nil
}
