package bannerdb

import (
	"avito-testcase/internal/service/createbanner"
	"database/sql"
	"errors"
)

type CreateBanner struct {
	tx *sql.Tx
	in *createbanner.Input
}

func NewCreateBanner(tx *sql.Tx, in *createbanner.Input) *CreateBanner {
	cb := &CreateBanner{
		tx: tx,
		in: in,
	}

	return cb
}

func (cb *CreateBanner) SendQuery() (*createbanner.Output, error) {
	out := &createbanner.Output{}

	var check CreateCheckQueryBuilder

	row := cb.tx.QueryRow(check.Build(cb.in))

	var cnt int
	err := row.Scan(&cnt)
	if !errors.Is(err, sql.ErrNoRows) && err != nil {
		return nil, err
	}

	if cnt > 0 {
		return nil, createbanner.ErrBannerInconsistency
	}

	_, err = cb.tx.Exec("INSERT INTO features(id) VALUES ($1) ON CONFLICT DO NOTHING", *cb.in.FeatureID)
	if err != nil {
		return nil, err
	}

	row = cb.tx.QueryRow(
		`INSERT INTO banners(content, feature_id, created_at, updated_at, is_active) 
					VALUES($1, $2, now(), now(), $3) RETURNING id`, *cb.in.Content, *cb.in.FeatureID, *cb.in.IsActive)

	if err = row.Scan(&out.BannerID); err != nil {
		return nil, err
	}

	for _, k := range *cb.in.TagIDs {
		_, err = cb.tx.Exec("INSERT INTO tags(id) VALUES ($1) ON CONFLICT DO NOTHING", k)
		if err != nil {
			return nil, err
		}

		_, err = cb.tx.Exec("INSERT INTO tag_banner(tag_id, banner_id) VALUES($1,$2) ON CONFLICT DO NOTHING", k, out.BannerID)
		if err != nil {
			return nil, err
		}
	}

	return out, nil
}
