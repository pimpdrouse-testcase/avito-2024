package bannerdb

import (
	"avito-testcase/internal/service/updatebanner"
	"fmt"
	"strconv"
	"strings"
)

type UpdateQueryBuilder struct {
}

func (uqb UpdateQueryBuilder) Build(data *updatebanner.Input) string {
	var queryBuilder strings.Builder

	queryBuilder.WriteString(uqb.initQuery())
	queryBuilder.WriteString(uqb.featureSet(data.FeatureID))
	queryBuilder.WriteString(uqb.contentSet(data.Content))
	queryBuilder.WriteString(uqb.isActiveSet(data.IsActive))
	queryBuilder.WriteString(uqb.bannerFilter(data.ID))

	return queryBuilder.String()
}

func (uqb UpdateQueryBuilder) initQuery() string {
	return `UPDATE banners SET updated_at = now() `
}

func (uqb UpdateQueryBuilder) featureSet(featureID *int) string {
	if featureID == nil {
		return ""
	}

	builder := strings.Builder{}

	builder.WriteString(", feature_id = ")
	builder.WriteString(strconv.Itoa(*featureID))
	builder.WriteString(" ")

	return builder.String()
}

func (uqb UpdateQueryBuilder) contentSet(content *string) string {
	if content == nil {
		return ""
	}
	builder := strings.Builder{}
	builder.WriteString(", content = '")
	builder.WriteString(*content)
	builder.WriteString("' ")
	return builder.String()
}

func (uqb UpdateQueryBuilder) isActiveSet(active *bool) string {
	if active == nil {
		return ""
	}

	builder := strings.Builder{}
	builder.WriteString(", is_active = ")
	builder.WriteString(strconv.FormatBool(*active))
	builder.WriteString(" ")
	return builder.String()
}

func (uqb UpdateQueryBuilder) bannerFilter(id int) string {
	return fmt.Sprintf("WHERE id = %v", id)
}
