package bannerdb

import (
	"avito-testcase/internal/service/updatebanner"
	"fmt"
	"strconv"
	"strings"
)

type UpdateCheckQueryBuilder struct {
}

func (uqb UpdateCheckQueryBuilder) Build(data *updatebanner.Input) string {
	var queryBuilder strings.Builder

	queryBuilder.WriteString(uqb.initQuery())
	queryBuilder.WriteString(uqb.featureFilter(data.FeatureID))
	queryBuilder.WriteString(uqb.tagsFilter(data.TagIDs))
	queryBuilder.WriteString(uqb.bannerFilter(data.ID))

	return queryBuilder.String()
}

func (uqb UpdateCheckQueryBuilder) initQuery() string {
	return `SELECT count(*) FROM banners bans `
}

func (uqb UpdateCheckQueryBuilder) featureFilter(featureID *int) string {
	featureBuilder := strings.Builder{}

	featureBuilder.WriteString("JOIN features feat ON bans.feature_id = feat.id AND feat.id = ")
	featureBuilder.WriteString(strconv.Itoa(*featureID))
	featureBuilder.WriteString(" ")

	return featureBuilder.String()
}

func (uqb UpdateCheckQueryBuilder) tagsFilter(tagsID *[]int) string {
	tagsBuilder := strings.Builder{}
	tagsBuilder.WriteString("JOIN tag_banner tag_ban ON bans.id = tag_ban.banner_id AND tag_ban.tag_id in ( ")
	for i, k := range *tagsID {
		tagsBuilder.WriteString(strconv.Itoa(k))
		if i == len(*tagsID)-1 {
			break
		}
		tagsBuilder.WriteString(",")
	}
	tagsBuilder.WriteString(") ")
	return tagsBuilder.String()
}

func (ubq UpdateCheckQueryBuilder) bannerFilter(id int) string {
	return fmt.Sprintf("WHERE bans.id != %v", id)
}
