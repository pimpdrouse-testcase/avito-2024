package bannerdb

import (
	"avito-testcase/internal/service/readbanner"
	"strconv"
	"strings"
)

type AdminQueryBuilder struct {
}

func (aqb AdminQueryBuilder) Build(data *readbanner.Input) string {
	var queryBuilder strings.Builder

	queryBuilder.WriteString(aqb.initQuery())
	queryBuilder.WriteString(aqb.featureFilter(data.FeatureID))
	queryBuilder.WriteString(aqb.tagFilter(data.TagID))
	queryBuilder.WriteString(aqb.limit(data.Limit))
	queryBuilder.WriteString(aqb.offset(data.Offset))

	return queryBuilder.String()
}

func (aqb AdminQueryBuilder) initQuery() string {
	return `SELECT DISTINCT ON (bans.id) bans.id , bans.feature_id, bans.content, bans.is_active, bans.created_at, bans.updated_at 
			FROM banners bans `
}

func (aqb AdminQueryBuilder) featureFilter(featureID *int) string {
	featureBuilder := strings.Builder{}
	featureBuilder.WriteString("JOIN features feat ON bans.feature_id = feat.id ")
	if featureID != nil {
		featureBuilder.WriteString("AND feat.id = ")
		featureBuilder.WriteString(strconv.Itoa(*featureID))
		featureBuilder.WriteString(" ")
	}

	return featureBuilder.String()
}

func (aqb AdminQueryBuilder) tagFilter(tagID *int) string {
	featureBuilder := strings.Builder{}
	featureBuilder.WriteString("JOIN tag_banner tag_ban ON bans.id = tag_ban.banner_id ")
	if tagID != nil {
		featureBuilder.WriteString("AND tag_ban.tag_id = ")
		featureBuilder.WriteString(strconv.Itoa(*tagID))
		featureBuilder.WriteString(" ")
	}

	return featureBuilder.String()
}

func (aqb AdminQueryBuilder) limit(limit *int) string {
	limitBuilder := strings.Builder{}
	if limit != nil {
		limitBuilder.WriteString("LIMIT ")
		limitBuilder.WriteString(strconv.Itoa(*limit))
		limitBuilder.WriteString(" ")
	}

	return limitBuilder.String()
}

func (aqb AdminQueryBuilder) offset(offset *int) string {
	offsetBuilder := strings.Builder{}
	if offset != nil {
		offsetBuilder.WriteString("OFFSET ")
		offsetBuilder.WriteString(strconv.Itoa(*offset))
		offsetBuilder.WriteString(" ")
	}

	return offsetBuilder.String()
}
