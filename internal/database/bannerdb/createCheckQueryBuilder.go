package bannerdb

import (
	"avito-testcase/internal/service/createbanner"
	"strconv"
	"strings"
)

type CreateCheckQueryBuilder struct {
}

func (ccqb CreateCheckQueryBuilder) Build(data *createbanner.Input) string {
	var queryBuilder strings.Builder

	queryBuilder.WriteString(ccqb.initQuery())
	queryBuilder.WriteString(ccqb.tagsFilter(data.TagIDs))
	queryBuilder.WriteString(ccqb.featureFilter(data.FeatureID))

	return queryBuilder.String()
}

func (ccqb CreateCheckQueryBuilder) initQuery() string {
	return `SELECT count(*) FROM banners bans `
}

func (ccqb CreateCheckQueryBuilder) tagsFilter(tagsID *[]int) string {
	tagsBuilder := strings.Builder{}
	tagsBuilder.WriteString("JOIN tag_banner tag_ban ON bans.id = tag_ban.banner_id AND tag_ban.tag_id in ( ")
	for i, k := range *tagsID {
		tagsBuilder.WriteString(strconv.Itoa(k))
		if i == len(*tagsID)-1 {
			break
		}
		tagsBuilder.WriteString(",")
	}
	tagsBuilder.WriteString(") ")
	return tagsBuilder.String()
}

func (ccqb CreateCheckQueryBuilder) featureFilter(featureID *int) string {
	featureBuilder := strings.Builder{}
	featureBuilder.WriteString("WHERE  bans.feature_id = ")
	featureBuilder.WriteString(strconv.Itoa(*featureID))
	featureBuilder.WriteString(" ")

	return featureBuilder.String()
}
