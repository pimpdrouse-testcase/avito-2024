package bannerdb

import (
	"avito-testcase/internal/service/updatebanner"
	"database/sql"
	"errors"
)

type UpdateBanner struct {
	tx *sql.Tx
	in *updatebanner.Input
}

func NewUpdateBanner(tx *sql.Tx, in *updatebanner.Input) *UpdateBanner {
	ub := &UpdateBanner{
		tx: tx,
		in: in,
	}

	return ub
}

func (ub *UpdateBanner) SendQuery() error {
	if err := ub.checkBannerExists(); err != nil {
		return err
	}

	if err := ub.fillFeatureID(); err != nil {
		return err
	}

	if err := ub.fillTagsID(); err != nil {
		return err
	}

	var check UpdateCheckQueryBuilder

	row := ub.tx.QueryRow(check.Build(ub.in))

	var cnt int
	err := row.Scan(&cnt)
	if !errors.Is(err, sql.ErrNoRows) && err != nil {
		return err
	}

	if cnt > 0 {
		return updatebanner.ErrUpdateBannerInconsistency
	}

	var query UpdateQueryBuilder

	_, err = ub.tx.Exec(query.Build(ub.in))
	if err != nil {
		return err
	}

	if err := ub.updateTags(); err != nil {
		return err
	}

	return nil
}

func (ub *UpdateBanner) checkBannerExists() error {
	var a int
	row := ub.tx.QueryRow(`SELECT 1 FROM banners WHERE id = $1`, ub.in.ID)
	err := row.Scan(&a)
	if errors.Is(err, sql.ErrNoRows) {
		return updatebanner.ErrBannerNotFound
	}
	return err
}

func (ub *UpdateBanner) fillFeatureID() error {
	if ub.in.FeatureID != nil {
		return nil
	}

	row := ub.tx.QueryRow("SELECT feature_id FROM banners WHERE id = $1", ub.in.ID)
	return row.Scan(&ub.in.FeatureID)
}

func (ub *UpdateBanner) fillTagsID() error {
	if ub.in.TagIDs != nil {
		return nil
	}

	rows, err := ub.tx.Query("SELECT tag_id FROM tag_banner WHERE banner_id = $1", ub.in.ID)
	if err != nil {
		return err
	}

	var tagsID []int

	for rows.Next() {
		var tagID int

		if err := rows.Scan(&tagID); err != nil {
			return err
		}

		tagsID = append(tagsID, tagID)
	}

	ub.in.TagIDs = &tagsID

	return nil
}

func (ub *UpdateBanner) updateTags() error {
	_, err := ub.tx.Exec("DELETE FROM tag_banner WHERE banner_id=$1", ub.in.ID)
	if err != nil {
		return err
	}

	for _, k := range *ub.in.TagIDs {
		_, err = ub.tx.Exec("INSERT INTO tag_banner(tag_id, banner_id) VALUES ($1,$2)", k, ub.in.ID)
		if err != nil {
			return err
		}
	}

	return nil
}
