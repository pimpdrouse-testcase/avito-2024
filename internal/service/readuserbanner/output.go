package readuserbanner

type Output map[string]interface{}

func NewOutput() Output {
	return make(Output)
}
