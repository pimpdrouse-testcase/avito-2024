package readuserbanner

type Controller struct {
	cache   Cache
	storage Storage
}

func NewController(cache Cache, store Storage) *Controller {
	rc := &Controller{
		cache:   cache,
		storage: store,
	}

	return rc
}

func (ctl *Controller) Read(in *Input, role string) (Output, error) {
	var out Output

	if *in.UseLastRevision {
		return ctl.storage.Read(in, role)
	}

	out, ok := ctl.cache.Get(in, role)
	if ok {
		return out, nil
	}

	out, err := ctl.storage.Read(in, role)
	if err != nil {
		return nil, err
	}

	ctl.cache.Set(in, role, out)

	return out, nil
}
