package readuserbanner

import "errors"

var ErrBannerNotFound = errors.New("banner for user not found")

type Cache interface {
	Get(data *Input, role string) (Output, bool)
	Set(data *Input, role string, banner Output)
}

type Storage interface {
	Read(in *Input, role string) (Output, error)
}
