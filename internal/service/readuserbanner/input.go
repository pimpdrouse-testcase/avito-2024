package readuserbanner

import (
	"encoding/json"
	"github.com/go-playground/validator/v10"
)

type Input struct {
	TagID           *int  `form:"tag_id" validate:"required,gte=0,number"`
	FeatureID       *int  `form:"feature_id" validate:"required,gte=0,number"`
	UseLastRevision *bool `form:"use_last_revision,default=false" validate:"omitempty,boolean"`
}

func NewInput(tagID, featureID int, userLastRevision bool) *Input {
	rd := &Input{
		TagID:           &tagID,
		FeatureID:       &featureID,
		UseLastRevision: &userLastRevision,
	}

	return rd
}

func (in *Input) Validate() error {
	return validator.New().Struct(in)
}

func (in *Input) String() string {
	bytes, _ := json.Marshal(in)
	return string(bytes)
}
