package createbanner

import "github.com/go-playground/validator/v10"

type Input struct {
	TagIDs    *[]int  `json:"tag_ids" validate:"required,gt=0,dive,gte=0,number"`
	FeatureID *int    `json:"feature_id" validate:"omitempty,gte=0,number"`
	Content   *string `json:"content" validate:"required,json"`
	IsActive  *bool   `json:"is_active" validate:"required,boolean"`
}

func NewInput() *Input {
	return &Input{}
}
func (in *Input) Validate() error {
	if in.TagIDs == nil && in.FeatureID == nil && in.Content == nil && in.IsActive == nil {
		return ErrEmptyCreate
	}
	return validator.New().Struct(in)
}
