package createbanner

import "errors"

var (
	ErrUserForbidden       = errors.New("user with not admin role forbidden")
	ErrBannerInconsistency = errors.New("create query make data inconsistency")
	ErrEmptyCreate         = errors.New("empty struct for create")
)

type Storage interface {
	Create(data *Input) (*Output, error)
}
