package createbanner

type Controller struct {
	store Storage
}

func NewController(storage Storage) *Controller {
	dc := &Controller{
		store: storage,
	}

	return dc
}

func (dc *Controller) Create(input *Input, role string) (*Output, error) {
	if role != "admin" {
		return nil, ErrUserForbidden
	}

	return dc.store.Create(input)
}
