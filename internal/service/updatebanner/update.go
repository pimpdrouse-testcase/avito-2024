package updatebanner

import "errors"

var (
	ErrUserForbidden             = errors.New("user with not admin role forbidden")
	ErrBannerNotFound            = errors.New("banner not found")
	ErrUpdateBannerInconsistency = errors.New("create query make data inconsistency")
	ErrEmptyUpdate               = errors.New("empty struct for update")
)

type Storage interface {
	Update(in *Input) error
}
