package updatebanner

import (
	"github.com/go-playground/validator/v10"
)

type Input struct {
	ID        int     `json:"-" validate:"required,gte=0,number"`
	TagIDs    *[]int  `json:"tag_ids" validate:"omitnil,gt=0,dive,gte=0,number"`
	FeatureID *int    `json:"feature_id" validate:"omitnil,gte=0,number"`
	Content   *string `json:"content" validate:"omitnil,json"`
	IsActive  *bool   `json:"is_active" validate:"omitnil,boolean"`
}

func NewInput(id int) *Input {
	cd := &Input{
		ID: id,
	}
	return cd
}
func (in *Input) Validate() error {
	if in.TagIDs == nil && in.FeatureID == nil && in.Content == nil && in.IsActive == nil {
		return ErrEmptyUpdate
	}
	return validator.New().Struct(in)
}
