package updatebanner

type Controller struct {
	store Storage
}

func NewController(storage Storage) *Controller {
	ctl := &Controller{
		store: storage,
	}

	return ctl
}

func (ctl *Controller) Update(input *Input, role string) error {
	if role != "admin" {
		return ErrUserForbidden
	}

	return ctl.store.Update(input)
}
