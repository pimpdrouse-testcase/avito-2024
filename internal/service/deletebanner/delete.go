package deletebanner

import "errors"

var (
	ErrUserForbidden  = errors.New("user with not admin role forbidden")
	ErrBannerNotFound = errors.New("banner not found")
)

type Storage interface {
	Delete(*Input) error
}
