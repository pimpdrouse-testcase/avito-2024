package deletebanner

type Controller struct {
	store Storage
}

func NewController(storage Storage) *Controller {
	dc := &Controller{
		store: storage,
	}

	return dc
}

func (dc *Controller) Delete(input *Input, role string) error {
	if role != "admin" {
		return ErrUserForbidden
	}

	return dc.store.Delete(input)
}
