package deletebanner

import "github.com/go-playground/validator/v10"

type Input struct {
	BannerID int `json:"tag_ids" validate:"required,gte=0,number"`
}

func NewInput(bannerID int) *Input {
	cd := &Input{
		BannerID: bannerID,
	}

	return cd
}

func (g *Input) Validate() error {
	return validator.New().Struct(g)
}
