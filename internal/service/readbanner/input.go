package readbanner

import (
	"encoding/json"
	"github.com/go-playground/validator/v10"
)

type Input struct {
	TagID     *int `form:"tag_id" validate:"omitnil,number,gte=0"`
	FeatureID *int `form:"feature_id" validate:"omitnil,number,gte=0"`
	Limit     *int `form:"limit" validate:"omitnil,number,gte=0"`
	Offset    *int `form:"offset" validate:"omitnil,number,gte=0"`
}

func NewInput() *Input {
	return &Input{}
}

func (in *Input) Validate() error {
	return validator.New().Struct(in)
}

func (in *Input) String() string {
	bytes, _ := json.Marshal(in)
	return string(bytes)
}
