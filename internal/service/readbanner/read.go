package readbanner

type Cache interface {
	Get(data *Input, role string) (*Output, bool)
	Set(data *Input, role string, banner *Output)
}

type Storage interface {
	Read(data *Input, role string) (*Output, error)
}
