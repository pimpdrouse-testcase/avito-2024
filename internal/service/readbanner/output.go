package readbanner

import (
	"github.com/go-playground/validator/v10"
	"time"
)

type Banner struct {
	BannerID  int                    `json:"banner_id" validate:"required,number,gte=0"`
	TagIDs    []int                  `json:"tag_ids" validate:"required,gte=1,dive,number,gte=0"`
	FeatureID int                    `json:"feature_id" validate:"required,number,gte=0"`
	Content   map[string]interface{} `json:"content"`
	IsActive  bool                   `json:"is_active" validate:"required,boolean"`
	CreatedAt time.Time              `json:"created_at"`
	UpdatedAt time.Time              `json:"updated_at"`
}

type Output []Banner

func (bans *Output) Validate() error {
	return validator.New(validator.WithRequiredStructEnabled()).Struct(bans)
}
