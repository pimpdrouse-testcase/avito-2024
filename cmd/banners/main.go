package main

import (
	"avito-testcase/internal/cache"
	"avito-testcase/internal/config"
	"avito-testcase/internal/database/bannerdb"
	"avito-testcase/internal/database/migration"
	"avito-testcase/internal/database/userbannerdb"
	"avito-testcase/internal/service/createbanner"
	"avito-testcase/internal/service/deletebanner"
	"avito-testcase/internal/service/readbanner"
	"avito-testcase/internal/service/readuserbanner"
	"avito-testcase/internal/service/updatebanner"
	"avito-testcase/internal/transport/http/handlers"
	"avito-testcase/internal/transport/http/middlewares"
	bannerMiddleware "avito-testcase/internal/transport/http/middlewares/banner"
	"avito-testcase/internal/transport/http/middlewares/userbanner"
	"context"
	"errors"
	"github.com/gin-contrib/graceful"
	"github.com/gin-gonic/gin"
	"log/slog"
	"os"
	"os/signal"
	"syscall"
)

func main() {
	cfg, err := initConfig()
	if err != nil {
		panic(err)
	}

	if err = migration.NewMigration(cfg.PgURL).Migrate(); err != nil {
		panic(err)
	}

	ctx, stop := signal.NotifyContext(context.Background(), os.Interrupt, os.Kill, syscall.SIGTERM)
	defer stop()

	eng, err := graceful.Default(graceful.WithAddr(cfg.AppAddress))
	if err != nil {
		panic(err)
	}

	initBannerRoutes(ctx, eng, cfg)

	initUserBannerRoutes(ctx, eng, cfg)

	if err := eng.RunWithContext(ctx); !errors.Is(err, context.Canceled) && err != nil {
		panic(err)
	}
}

func initConfig() (*config.Config, error) {
	if _, ok := os.LookupEnv("CONFIG_PATH"); !ok {
		return nil, errors.New("not found env CONFIG_PATH")
	}

	cfgPath := os.Getenv("CONFIG_PATH")

	fl, err := os.Open(cfgPath)
	if err != nil {
		return nil, err
	}
	defer func() {
		if err := fl.Close(); err != nil {
			slog.Error("Close config file error: ", err)
		}
	}()

	cfg, err := config.NewConfig(fl)
	if err != nil {
		return nil, err
	}

	if err = cfg.Validate(); err != nil {
		return nil, err
	}

	return cfg, nil
}

func initBannerRoutes(ctx context.Context, route gin.IRouter, conf *config.Config) {
	bannerCache := cache.NewBannerCache(ctx, conf.CacheTTL, conf.CacheClearInterval)
	bannerStore := bannerdb.NewPostgresBanner(conf.PgURL)

	bh := handlers.NewBannerHandler(
		readbanner.NewController(bannerCache, bannerStore),
		createbanner.NewController(bannerStore),
		updatebanner.NewController(bannerStore),
		deletebanner.NewController(bannerStore),
	)

	banner := route.Group("/banner").Use(middlewares.TokenValidator)

	banner.GET("", bannerMiddleware.GetValidation, bh.Get)

	banner.POST("", bannerMiddleware.PostValidation, bh.Post)

	banner.PATCH("/:id", bannerMiddleware.PatchValidation, bh.Patch)

	banner.DELETE("/:id", bannerMiddleware.DeleteValidation, bh.Delete)
}

func initUserBannerRoutes(ctx context.Context, route gin.IRouter, conf *config.Config) {
	userBannerCache := cache.NewUserBannerCache(ctx, conf.CacheTTL, conf.CacheClearInterval)
	userBannerStore := userbannerdb.NewPostgresUserBanner(conf.PgURL)

	ubh := handlers.NewUserBannerHandler(
		readuserbanner.NewController(userBannerCache, userBannerStore),
	)

	userBanner := route.Group("/user_banner").Use(middlewares.TokenValidator)

	userBanner.GET("", userbanner.GetValidation, ubh.Get)
}
