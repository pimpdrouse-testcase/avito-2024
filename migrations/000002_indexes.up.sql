CREATE INDEX IF NOT EXISTS tag_id_idx ON tag_banner (tag_id);

CREATE INDEX IF NOT EXISTS tag_banner_idx ON tag_banner (tag_id, banner_id);

CREATE INDEX IF NOT EXISTS feature_id_idx ON banners (feature_id);