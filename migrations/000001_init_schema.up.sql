CREATE TABLE IF NOT EXISTS  features
(
    id serial NOT NULL PRIMARY KEY
);

CREATE TABLE IF NOT EXISTS  banners
(
    id serial NOT NULL PRIMARY KEY,
    content json NOT NULL,
    feature_id int REFERENCES features(id),
    created_at timestamp NOT NULL,
    updated_at timestamp NOT NULL,
    is_active boolean NOT NULL
);

CREATE TABLE IF NOT EXISTS  tags
(
    id serial NOT NULL PRIMARY KEY
);

CREATE TABLE IF NOT EXISTS tag_banner(
    id serial NOT NULL PRIMARY KEY,
    tag_id int REFERENCES tags(id),
    banner_id int REFERENCES banners(id)
);