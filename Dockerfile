FROM golang:alpine AS build

WORKDIR /app

COPY ./go.mod ./go.sum ./
RUN go mod download

COPY ./ ./
RUN go build -o /out/server ./cmd/banners

FROM alpine

ENV CONFIG_PATH=config.yaml

COPY --from=build ./out/server ./app/config.yaml ./
COPY --from=build ./app/migrations ./migrations

CMD ["./server"]
