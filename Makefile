VERSION=dev
PG_URL='postgresql://avito:avito@0.0.0.0:5432/avito?sslmode=disable'

migration:
	migrate -path ./migrations/ -database ${PG_URL} up

lints:
	golangci-lint run ./...

test:
	go test ./...

build: test
	docker build -t avito-testcase:$(VERSION) .